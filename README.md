Carousel
========

Simple carousel library.

## Installation

  npm install git+ssh://git@gitlab.com:glue-gl/carousel.git --save

## Usage

  Carousel = require('carousel')
  instance = Carousel boxDiv, sliderDiv
  instance.next()

## Contributing

To help with development, start the quick edit mode by running

  grunt dev

The browser window should appear with the page, live-reloading with any changes.
Happy coding!
